extends Node


export(Array) var SOUND_FILEPATHS = []
export(bool) var PITCH_RAND = true
export(float) var PITCH_RAND_RANGE = 0.1
export(float) var MIN_CALL_DELAY = 0.2
export(float) var DB_GAIN = 0

var rng : RandomNumberGenerator
var can_play : bool = true

func play():
	var selected : String = SOUND_FILEPATHS[rng.randi_range(0,len(SOUND_FILEPATHS)-1)]
	var pitch_scale : float = 1.0+rng.randf_range(-PITCH_RAND_RANGE/2.0,PITCH_RAND_RANGE/2.0) if PITCH_RAND else 1.0
	
	if can_play:
		$player.set_stream(load(selected))
		$player.set_pitch_scale(pitch_scale)
		$player.set_volume_db(DB_GAIN)
		$player.play()
		
		if MIN_CALL_DELAY!=0:
			can_play=false
			$SampleCallTimer.set_wait_time(MIN_CALL_DELAY)
			$SampleCallTimer.start()

func stop():
	$player.stop()

func _ready():
	rng = RandomNumberGenerator.new()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_SampleCallTimer_timeout():
	can_play=true
