# Halfway

Submitted for [Global Game Jam 2022](https://globalgamejam.org/2022/games/halfway-0).
Itch.io page [here](https://amargs.itch.io/halfway)

# Description 
Short puzzle-platformer featuring two characters achieving the same goal with different approaches.

All assets, music and code were developed by the team.

## Credits
Created by: André Pinho, Dinis Canastro, Margarida Silva, Ricardo Chaves, Ricardo Louro, Rodrigo Pereira
