extends Area2D

var triggered = false

func start_dialogue():
	var dialogue_player = preload("res://text/DialoguePlayer.tscn").instance()
	get_tree().get_current_scene().add_child(dialogue_player)

	if dialogue_player:
		dialogue_player.play()

func _on_DialogueArea_body_entered(body):
	if triggered or not body.is_in_group("Player"):
		return

	start_dialogue()
	triggered = true
