extends Node

const LevelTransitions = {
	"Start" : "Level1A",
	"Level1A": "Level1B",
	"Level1B": "Level2A",
	"Level2A": "Level2B",
	"Level2B": "Level3",
	"Level3": "Start",
}

func handle_master_volume():

	var dB_inc : float = 2.5
	if Input.is_action_just_pressed("volume_up"):
		AudioServer.set_bus_volume_db(0,AudioServer.get_bus_volume_db(0)+dB_inc)
	elif Input.is_action_just_pressed("volume_down"):
		AudioServer.set_bus_volume_db(0,AudioServer.get_bus_volume_db(0)-dB_inc)

func _ready():
	var asp=AudioStreamPlayer.new()
	add_child(asp)
	asp.set_stream(load('res://assets/music/2_release_0.mp3'))
	asp.play()

func _process(_delta):
	if Input.is_action_pressed("exit"):
		get_tree().quit()
	if Input.is_action_pressed("reset"):
		var _ret = get_tree().reload_current_scene()

	handle_master_volume()
