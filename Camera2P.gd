extends Camera2D

export(int, "Youngling", "Oldlang") var character_name # 0 - Youngling, 1 - Oldlang
onready var player = get_parent().get_node("Player")
onready var player2 = get_parent().get_node("Player2")

func _process(_delta):
	if character_name == 0:
		position.x = player.position.x
	elif character_name == 1:
		position.x = player2.position.x
	#position.y = player.position.y

func swap_camera(trgt_character):
	if trgt_character == "Youngling":
		character_name = 1
	elif trgt_character == "Oldang":
		character_name = 0
