extends ParallaxLayer


export(float) var CLOUD_SPEED = -10

# Called when the node enters the scene tree for the first time.
func _process(delta):
	self.motion_offset.x += CLOUD_SPEED * delta

