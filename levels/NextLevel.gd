extends Sprite

export(bool) var GOAL_DEFINED = false
export(String, "Youngling", "Oldang") var character_name = "Youngling"

func _on_Hitbox_body_entered(body: Node):
	if not body.is_in_group("Player"):
		return
	
	if !GOAL_DEFINED:
		transit()
	else:
		if body.has_meta("Character"):
			print("Meta: " + body.get_meta("Character"))
			print("Local: " + character_name)
			if body.get_meta("Character") == character_name:
				transit()
			
func transit():
	var current_scene = get_tree().get_current_scene().get_name()
	if not Globals.LevelTransitions.has(current_scene):
		return

	var next_scene = Globals.LevelTransitions[current_scene]
	#print("current: "+current_scene+" next: "+next_scene)
	var next_scene_path = "res://levels/" + next_scene + ".tscn"
	var _ret = get_tree().change_scene(next_scene_path)
		
