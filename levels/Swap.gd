extends Area2D

export(String, "Youngling", "Oldang") var trigger_character # 0 - Youngling, 1 - Oldlang

onready var camera = get_parent().get_node("Camera")
onready var player = get_parent().get_node("Player")
onready var player2 = get_parent().get_node("Player2")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	# Check if the right player got inside
	for o in get_overlapping_bodies():
		if o.has_meta("Character"):
			if o.get_meta("Character") == trigger_character:
				# Change camera to the other
				camera.swap_camera(trigger_character)
				# Change controls to the other
				player.switch_controllable(trigger_character)
				player2.switch_controllable(trigger_character)
				# Turn off the collision so it can't be activated again
				$CollisionShape2D.disabled = true
