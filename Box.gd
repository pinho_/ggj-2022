extends KinematicBody2D

export(float) var MASS = 25.0
export(float) var DECEL = 2.0
export(float) var G_ACCEL = 17500.0
export(float) var AIR_DRAG = 1.0
export(float) var FLOOR_DRAG = 5.0

var vel : Vector2 = Vector2()
var grabbed = false

var side : float

func _ready():
	set_meta("AmIABox", true)


func _physics_process (delta):
	if !grabbed: # If it is grabbed, it can ignore the box physics calculation
		
		# Drag calculation
		var current_drag : float = FLOOR_DRAG if(is_on_floor()) else AIR_DRAG
		var d_decel : float = DECEL * delta * current_drag
		vel.x -= vel.x * d_decel
		
		# Gravity
		var d_g_accel : float = G_ACCEL * delta
		vel.y+=d_g_accel*delta
		
		vel = move_and_slide(vel, Vector2.UP)
		
		

func get_grabbed ():
	grabbed = true
	get_node("CollisionShape2D").disabled = true

func get_yanked_son (direction):
	grabbed = false
	get_node("CollisionShape2D").disabled = false
	vel.x += direction.x
	vel.y += direction.y
	vel = move_and_slide(vel, Vector2.UP)
	

