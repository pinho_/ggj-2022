extends KinematicBody2D

export(float) var ACCEL = 2.0
export(float) var DECEL = 14.0
export(float) var G_ACCEL = 17500.0
export(float) var MAX_VAL_X = 200.0
export(float) var JUMP_VEL = 200.0
export(float) var AIRTIME_DEMULT = 0.35
export(float) var BOUNCE_BOOST = 7.5
export(float) var BOUNCE_VEL_SAT = 50.0
export(float) var DIR_CROSSOVER = 40.0
export(float) var WALLJUMP_TIMER = 0.4

export(int) var GRAB_DELAY = 30 
export(float) var YEET_STRENGTH_X = 200
export(float) var YEET_STRENGTH_Y = 100
export(bool) var INVERTED = false
export(bool) var CONTROLLABLE = true

export(Array) var YOUNGLING_WALK_SOUND
export(Array) var YOUNGLING_LAND_SOUND
export(String) var YOUNGLING_JUMP_SOUND

export(Array) var OLDLING_WALK_SOUND
export(Array) var OLDLING_LAND_SOUND
export(String) var OLDLING_JUMP_SOUND

#TODO Set jump timeout after landing?

var vel : Vector2 = Vector2()

export(int, "Youngling", "Oldlang") var character_name # 0 - Youngling, 1 - Oldlang
var walljump_enable = false
var pickups_enable = false
var can_move = true

var can_wall_jump : bool = false
var last_collision : KinematicCollision2D = null

var sprite : Sprite
var sprite_animation : AnimatedSprite

func is_youngling() -> bool:
	return character_name == 0
	
var interactable_cache : KinematicBody2D = null 
var grabbed_object : KinematicBody2D = null 
var yank_direction : Vector2
var box_list : Array
var last_throw_frametimer : int

func turn(direction : Vector2):
	if direction == Vector2.LEFT:
		sprite_animation.flip_h = is_youngling()
		sprite.flip_h = is_youngling()
	elif direction == Vector2.RIGHT:
		sprite_animation.flip_h = !is_youngling()
		sprite.flip_h = !is_youngling()

func set_animation(animation, on):
	if on:
		sprite.hide()
		sprite_animation.show()
		if sprite_animation.is_playing():
			sprite_animation.stop()
		sprite_animation.play(animation)
	else:
		sprite.show()
		sprite_animation.stop()
		sprite_animation.hide()

# TODO Deprecated, delete at the end
func is_below(orig, trgt):
	var direction = orig.direction_to(trgt)
	if direction.dot(Vector2.DOWN) > 0.75:
		return true
	return false

func landed_on_floor():
	var collision = get_last_slide_collision()
	if not collision:
		return false

	var landed = collision.normal == Vector2.UP
	return not last_collision and landed

func start_landing_smoke():
	var particles = preload("res://particles/LandingParticles.tscn").instance()
	particles.position = position
	for particle in particles.get_children():
		particle.emitting = true
	get_tree().get_current_scene().add_child(particles)

func start_running_smoke(flip):
	var particles = preload("res://particles/RunningParticles.tscn").instance()
	particles.position = position
	if flip:
		particles.scale = Vector2(-1, 1)
	for particle in particles.get_children():
		particle.emitting = true
	get_tree().get_current_scene().add_child(particles)

func start_falling_smoke():
	var particles = preload("res://particles/FallingParticles.tscn").instance()
	particles.position = position
	for particle in particles.get_children():
		particle.emitting = true
		particle.lifetime = 0.3
		particle.amount = 20
		var particle_material = particle.get_process_material()
		particle_material.gravity = Vector3(0, -G_ACCEL/1000, 0)
		particle.set_process_material(particle_material)
	get_tree().get_current_scene().add_child(particles)

func start_wall_smoke(flip):
	var particles = preload("res://particles/LandingParticles.tscn").instance()
	particles.position = position + Vector2(2, 0)
	particles.rotate(PI/2)
	if flip:
		particles.position += Vector2(25, 0)
	for particle in particles.get_children():
		particle.emitting = true
	get_tree().get_current_scene().add_child(particles)

func box_space_clear ():
	for o in $CanIBox.get_overlapping_bodies():
		if o.get_class() == "TileMap":
			return false
	return true
	
func get_yank_direction ():
	if sprite.flip_h == true:
		return Vector2(YEET_STRENGTH_X,-YEET_STRENGTH_Y)
	else:
		return Vector2(-YEET_STRENGTH_X,-YEET_STRENGTH_Y)
		
func switch_controllable(trgt_character):
	if trgt_character == get_meta("Character"):
		CONTROLLABLE = false
	else:
		CONTROLLABLE = true

func set_active(active):
	can_move = active

func init_sound_emitters(walk_sound,land_sound,jump_sound,walk_delay):
	#Walking sounds
	$SoundEmitters/WalkGrassSound.SOUND_FILEPATHS=walk_sound
	$SoundEmitters/WalkGrassSound.PITCH_RAND=true
	$SoundEmitters/WalkGrassSound.PITCH_RAND_RANGE=0.5
	$SoundEmitters/WalkGrassSound.MIN_CALL_DELAY=walk_delay
	$SoundEmitters/WalkGrassSound.DB_GAIN=0
	
	#Land sounds
	$SoundEmitters/LandGrassSound.SOUND_FILEPATHS=land_sound
	$SoundEmitters/LandGrassSound.PITCH_RAND=true
	$SoundEmitters/LandGrassSound.PITCH_RAND_RANGE=0.1
	$SoundEmitters/LandGrassSound.MIN_CALL_DELAY=0	
	$SoundEmitters/LandGrassSound.DB_GAIN=0
	
	#Jump sounds
	$SoundEmitters/JumpSound.SOUND_FILEPATHS=[jump_sound]
	$SoundEmitters/JumpSound.PITCH_RAND=true
	$SoundEmitters/JumpSound.PITCH_RAND_RANGE=0.1
	$SoundEmitters/JumpSound.MIN_CALL_DELAY=0
	$SoundEmitters/JumpSound.DB_GAIN=0	

func _ready ():
	$AnimatedYoungling.hide()
	$AnimatedOldlang.hide()
	if is_youngling():
		set_meta("Character", "Youngling")
		sprite = $Youngling
		sprite_animation = $AnimatedYoungling
		$Oldlang.hide()
	else:
		set_meta("Character", "Oldang")
		sprite = $Oldlang
		sprite_animation = $AnimatedOldlang
		$Youngling.hide()
	$WallJumpTimer.set_wait_time(WALLJUMP_TIMER)

	if is_youngling(): # Youngling
		walljump_enable = true
		init_sound_emitters(YOUNGLING_WALK_SOUND,YOUNGLING_LAND_SOUND,YOUNGLING_JUMP_SOUND,0.25)

	else: # Oldlang
		pickups_enable = true
		JUMP_VEL = 150.0
		init_sound_emitters(OLDLING_WALK_SOUND,OLDLING_LAND_SOUND,OLDLING_JUMP_SOUND,0.35)
		
func _physics_process (delta):
	if not can_move:
		return
	
	var moving_right : bool = false
	var moving_left : bool = false
	var jumping : bool = false
	
	if CONTROLLABLE:
		if !INVERTED:
			moving_right = Input.is_action_pressed("move_right")
			moving_left = Input.is_action_pressed("move_left")
		else:
			moving_left = Input.is_action_pressed("move_right")
			moving_right = Input.is_action_pressed("move_left")
		jumping = Input.is_action_just_pressed("jump")
	
	if moving_left || moving_right || jumping:
		interactable_cache = null

	#####
	#Handle x axis

	#Flip sprite
	if moving_left:
		turn(Vector2.LEFT)
		$GrabArea.set_rotation_degrees(-70)
	elif moving_right:
		turn(Vector2.RIGHT)
		$GrabArea.set_rotation_degrees(70)

	if is_on_floor():
		if sprite_animation.animation == "fall" && sprite_animation.is_playing():
			set_animation("fall", false)
		if jumping:
			set_animation("jump", true)
			$SoundEmitters/JumpSound.play()
		elif moving_left || moving_right:
			start_running_smoke(moving_left)
			$SoundEmitters/WalkGrassSound.play()
			if grabbed_object != null:
				if !sprite_animation.is_playing() || sprite_animation.animation != "run_with_box":
					set_animation("run_with_box", true)
			else:	
				if !sprite_animation.is_playing() || sprite_animation.animation != "run":
					set_animation("run", true)
		else:
			if sprite_animation.animation == "run":
				set_animation("run", false)
			if sprite_animation.animation == "run_with_box":
				set_animation("run_with_box", false)
			if sprite_animation.animation == "jump":
				set_animation("jump", false)
	else:
		if sprite_animation.animation == "run":
			set_animation("run", false)

	if vel.y > 100:
		if sprite_animation.animation != "fall":
			set_animation("fall", true)

	if vel.y > 150:
		start_falling_smoke()

	#Air demultiplier
	var _a_d : float = 1.0 if(is_on_floor() || is_on_wall()) else AIRTIME_DEMULT
	
	#Delta scalling and airtime compensator
	var d_accel : float = ACCEL * delta * _a_d
	var d_decel : float = DECEL * delta * _a_d
	var d_g_accel : float = G_ACCEL * delta
	
	if moving_right && !moving_left && vel.x >= -DIR_CROSSOVER:
		#Moving right
		vel.x+=(MAX_VAL_X-vel.x) * d_accel
		
	elif moving_left && !moving_right && vel.x <= DIR_CROSSOVER:
		#Moving left
		vel.x -= (MAX_VAL_X + vel.x) * d_accel
	else:
		#Not moving or changing direction
		vel.x -= vel.x * d_decel
	
	if landed_on_floor():
		set_animation("land", true)
		start_landing_smoke()
		$SoundEmitters/LandGrassSound.play()

	# Bounce of wall
	if is_on_wall() && !is_on_floor():
		var dir = vel.x/abs(vel.x) if vel.x!=0 else 1.0
		vel.x = - dir * min(abs(vel.x) * BOUNCE_BOOST,BOUNCE_VEL_SAT)

	# Handle wall jump time window
	last_collision=get_last_slide_collision()
	if walljump_enable:
		if is_on_wall():
			can_wall_jump=true
			$WallJumpTimer.start()

	if jumping && is_on_floor():
		#Normal jump
		vel.y = -JUMP_VEL
		$GrabArea.set_rotation_degrees(0)
	elif jumping && can_wall_jump && (moving_right || moving_left):
		#Wall jump
		if not last_collision:
			return
		if moving_right && last_collision.normal == Vector2.RIGHT:
			vel.x = JUMP_VEL
			vel.y = -JUMP_VEL
			can_wall_jump = false
			start_wall_smoke(false)
		elif moving_left && last_collision.normal == Vector2.LEFT:
			vel.x = -JUMP_VEL
			vel.y = -JUMP_VEL
			can_wall_jump = false
			start_wall_smoke(true)
		turn(last_collision.normal)
		$GrabArea.set_rotation_degrees(0)

	#Apply gravity
	vel.y+=d_g_accel*delta	
	vel = move_and_slide(vel, Vector2.UP)
	
	## Grabbing mechanic TODO Find a way to discard collisions from below, only above and from the sides
	if pickups_enable:
		if last_throw_frametimer == GRAB_DELAY:
			var min_distance : float = 10000.0
			for o in $GrabArea.get_overlapping_bodies(): # If various boxes, pick the closest
				if o.has_meta("AmIABox"):
					var temp : float = position.distance_to(o.get_position())
					if temp < min_distance:
						min_distance = temp
						interactable_cache = o

		# Collision processor
		if Input.is_action_just_pressed("pickup"): # Can only grab if you have space
			if sprite_animation.animation != "box_pickup" && !sprite_animation.is_playing():
				set_animation("box_pickup", true)
			if interactable_cache != null && grabbed_object == null && last_throw_frametimer == GRAB_DELAY: # Grabbing
				if box_space_clear():
					grabbed_object = interactable_cache
					interactable_cache = null
					grabbed_object.get_grabbed()
					$HypoteticalBox.disabled = false
				else:
					interactable_cache.get_yanked_son(get_yank_direction())
				
			elif grabbed_object != null: # Ungrabbing
				$HypoteticalBox.disabled = true
				grabbed_object.get_yanked_son(get_yank_direction())
				grabbed_object = null
				last_throw_frametimer = 0

		# Increase timer
		if last_throw_frametimer < GRAB_DELAY:
			last_throw_frametimer += 1
		
		# If I'm grabbing a object, it follows me around
		if grabbed_object != null:
			grabbed_object.set_position(position + Vector2(0,-30))


func _on_WallJumpTimer_timeout():
	can_wall_jump=false


func _on_AnimatedOldlang_animation_finished():
	if sprite_animation.animation == "box_pickup":
		set_animation("box_pickup", false)
